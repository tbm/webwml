#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été trouvés dans QEMU, un émulateur rapide de
processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2391">CVE-2016-2391</a>

<p>Zuozhi Fzz a découvert que eof_times dans la prise en charge de l’émulation
OHCI USB pourrait être utilisé pour provoquer un déni de service, à l'aide d'un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2392">CVE-2016-2392</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-2538">CVE-2016-2538</a>

<p>Qinghao Tang a trouvé un déréférencement de pointeur NULL et plusieurs
dépassements d'entiers dans la prise en charge de périphérique Net USB qui
pourrait permettre à des administrateurs de l’OS invité local de provoquer un
déni de service. Ces problèmes concernaient la gestion de messages de contrôle
NDIS distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2841">CVE-2016-2841</a>

<p>Yang Hongke a signalé une vulnérabilité de boucle infinie dans la prise en
 charge de l’émulation NIC NE2000.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2857">CVE-2016-2857</a>

<p>Liu Ling a trouvé un défaut dans les routines de sommes de contrôle IP de
QEMU. Des attaquants pourraient exploiter ce défaut pour provoquer le plantage
de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2858">CVE-2016-2858</a>

<p>Allocations basées sur le tas et arbitraires dans la prise en charge du
dorsal de générateur de nombres pseudo-aléatoires (PRNG).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4001">CVE-2016-4001</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4002">CVE-2016-4002</a>

<p>Oleksandr Bazhaniuk a signalé un dépassement de tampon dans l’émulation de
contrôleurs Ethernet Stellaris et MIPSnet. Des utilisateurs distants
malveillants pourraient utiliser ces problèmes pour provoquer le plantage de
QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4020">CVE-2016-4020</a>

<p>Donghai Zdh a signalé que QEMU gérait incorrectement l’accès au TPR (Task
Priority Register), permettant à des administrateurs de l’OS invité local
d’obtenir des informations sensibles de la mémoire de tas d’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4037">CVE-2016-4037</a>

<p>Du Shaobo a trouvé une vulnérabilité de boucle infinie dans la prise en
charge de l’émulation EHCI USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4439">CVE-2016-4439</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4441">CVE-2016-4441</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5238">CVE-2016-5238</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5338">CVE-2016-5338</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6351">CVE-2016-6351</a>

<p>Li Qiang a trouvé différents problèmes dans la prise en charge de l’émulation
du contrôleur Fast SCSI (FSC) 53C9X par QEMU. Cela permettait à des utilisateurs
privilégiés de l’OS invité local de provoquer des dénis de service ou
éventuellement d’exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4453">CVE-2016-4453</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4454">CVE-2016-4454</a>

<p>Li Qiang a signalé des problèmes dans la gestion du module VGA VMWare de QEMU,
qui pourraient être utilisés pour provoquer le plantage de QEMU ou d’obtenir des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4952">CVE-2016-4952</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-7421">CVE-2016-7421</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-7156">CVE-2016-7156</a>

<p>Li Qiang a signalé des défauts dans l’émulation paravirtuelle du bus SCSI de
VMware. Ces problèmes concernent un accès hors limites et des boucles infinies,
qui permettaient des utilisateurs privilégiés de l’OS invité local de provoquer
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5105">CVE-2016-5105</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5106">CVE-2016-5106</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5107">CVE-2016-5107</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5337">CVE-2016-5337</a>

<p>Li Qiang a découvert plusieurs problèmes dans la prise en charge de
l’émulation du contrôleur hôte de bus MegaRAID SAS 8708EM2. Ces problèmes
incluent une fuite d'informations de pile lors de la lecture de configuration et
une écriture et lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6834">CVE-2016-6834</a>

<p>Li Qiang a signalé une vulnérabilité de boucle infinie lors de la
fragmentation de paquets dans la prise en charge de la couche d’abstraction de
transport réseau. Des utilisateurs privilégiés de l’OS invité local pourraient
utiliser ce défaut pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6836">CVE-2016-6836</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-6888">CVE-2016-6888</a>

<p>Li Qiang a trouvé des problèmes dans la prise en charge de l’émulation de
carte réseau VMXNET3 de VMWare, concernant une fuite d'informations et un
dépassement d'entier dans l’initialisation de paquets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7116">CVE-2016-7116</a>

<p>Felix Wilhel a découvert un défaut de traversée de répertoires dans le système de
fichiers de Plan 9 (9pfs), exploitable par des utilisateurs privilégiés de
l’OS invité local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7155">CVE-2016-7155</a>

<p>Tom Victor et Li Qiang ont signalé une lecture hors limites et une boucle
infinie dans la prise en charge de l’émulation paravirtuelle du bus SCSI de
VMware.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7161">CVE-2016-7161</a>

<p>Hu Chaojian a signalé un dépassement de tas dans la prise en charge de
l’émulation xlnx.xps-ethernetlite. Des utilisateurs privilégiés
de l’OS invité local pourraient l’utiliser pour provoquer le plantage de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7170">CVE-2016-7170</a>

<p>Qinghao Tang et Li Qiang ont signalé un défaut dans le module VGA VMWare
de QEMU. Il pourrait à utilisé par des utilisateurs privilégiés de l’OS invité local
pour provoquer le plantage de QEMU à l'aide d'un accès hors limites
dans la mémoire de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7908">CVE-2016-7908</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-7909">CVE-2016-7909</a>

<p>Li Qiang a signalé une vulnérabilité de boucle infinie dans l’émulation du
contrôleur Fast Ethernet ColdFire et de PC-Net II (Am79C970A) d’AMD. Ces défauts
permettaient à des administrateurs de l’OS invité local de provoquer
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8909">CVE-2016-8909</a>

<p>Huawei PSIRT ont trouvé un vulnérabilité de boucle infinie dans la prise en
charge de l’émulation HDA d’INTEL, concernant le traitement de flux de tampon
DMA. Des utilisateurs privilégiés de l’OS invité local pourraient utiliser cette
vulnérabilité pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8910">CVE-2016-8910</a>

<p>Andrew Henderson a signalé une boucle infinie dans la prise en charge de
l’émulation du contrôleur Ethernet RTL8139. Des utilisateurs privilégiés de
l’OS invité local pourraient utiliser cette boucle pour provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9101">CVE-2016-9101</a>

<p>Li Qiang a signalé une fuite de mémoire dans la prise en charge de
l’émulation du contrôleur Ethernet i8255x (PRO100).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9102">CVE-2016-9102</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9103">CVE-2016-9103</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9104">CVE-2016-9104</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9105">CVE-2016-9105</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9106">CVE-2016-9106</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8577">CVE-2016-8577</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8578">CVE-2016-8578</a>

<p>Li Qiang a signalé divers problèmes de sécurité dans le système de fichiers
de Plan 9 (9pfs), incluant une fuite de mémoire d’hôte et un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10664">CVE-2017-10664</a>

<p>Déni de service dans le serveur qemu-nbd (QEMU Disk Network Block
Device).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10839">CVE-2018-10839</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17962">CVE-2018-17962</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17963">CVE-2018-17963</a>

<p>Daniel Shapira a signalé plusieurs dépassement d'entiers dans le traitement
de paquets les contrôleurs Ethernet émulés par QEMU. Ces problèmes pourraient
conduire à un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:2.1+dfsg-12+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1599.data"
# $Id: $
