#use wml::debian::translation-check translation="353fcd924e0635c217cdd18154ac54cbf788303a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Le projet Debian déplore la perte de Lucy Wayland</define-tag>
<define-tag release_date>2019-03-08</define-tag>
#use wml::debian::news
# $Id$

<p>
Le projet Debian a le regret d’annoncer la disparition d'un membre de sa
communauté. Lucy Wayland est décédée récemment.
</p>

<p>
Lucy contribuait au projet au sein de la communauté Debian de Cambridge
(Royaume-Uni), participant depuis plusieurs années à l'organisation des
Mini-DebConf de Cambridge.
</p>

<p>
Elle était une fervente défenseure de la diversité et de l'inclusion, et a
participé à la création de l'équipe de Debian en charge de la diversité,
œuvrant à accroître la visibilité des groupes sous-représentés et apportant
son soutien pour résoudre, au sein de la communauté, les problèmes en
rapport avec la diversité.
</p>

<p>
Le projet Debian honore son travail et son dévouement à Debian et au
logiciel libre. Nous n'oublierons pas ses contributions et la grande qualité
de son travail continuera à nous inspirer.
</p>

<h2>À propos de Debian</h2>

<p>
Le projet Debian est une association de développeurs de logiciels
libres qui offrent volontairement leur temps et leurs efforts pour
produire le système d'exploitation complètement libre Debian GNU/Linux.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
