<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for arbitrary code execution documented in <a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a> was
incomplete in the previous upload. A more exhaustive change was
implemented upstream and completely disables non-Mercurial
subrepositories unless users changed the subrepos.allowed setting.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.1.2-2+deb8u6.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1414-2.data"
# $Id: $
